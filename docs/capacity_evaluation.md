# Capacity Evaluation for Dump Storage in the Bubble Context

**Overview**

Small recollection of info related to storage and bubble's specs.

**How many dumps can we store in a Bubble?**

Size of some present backups(August 2024):

- 4.0M - vnetbox.gee.snmc.cec.eu.int_20240731T020121.sql.tar.gz
- 8.2M - vnetbox.isp.snmc.cec.eu.int_20240731T020143.sql.tar.gz
- 859K - vnetbox-110.lab.snmc.cec.eu.int_20240731T060209.sql.tar.gz
- 139K - vnetbox-130.lab.snmc.cec.eu.int_20240731T060147.sql.tar.gz
- 69K - vnetbox-140.lab.snmc.cec.eu.int_20240731T060142.sql.tar.gz

With 50GB storage, and small backup size(10MB), we have plenty of room at present time, this should not be a concern for now.

**Are all bubbles having the same setup?**

Link to Bubble information:

[https://intragate.ec.europa.eu/snet/wiki/index.php/System/todoOnPrem](https://intragate.ec.europa.eu/snet/wiki/index.php/System/todoOnPrem)

General bubble's specs:

```
vCPUs : 4
RAM : 8192
DISK : 50GB
```

"AWS bubbles" will have the same specs. Other ones is expected to be the same, but it could be some differences.

# Netbox Backup Agent Overview

## Presentation

Netbox Backup Agent is a service for managing database dumps:
* Create dumps freely
* Read a dump
* List available dumps
* Delete a dump if necessary
* Upload a dump file
* Restore a dump file

Some documentation about how to use, into [how_to.md](how_to.md) file.

Its primary use is to manage PostgreSQL database dumps used for the persistence of a Netbox instance. However, it is possible to use this service with any other PostgreSQL database.

Netbox Backup Agent is installed in a [Netbox](https://netboxlabs.com/oss/netbox/) + [Netbox Docker Plugin](https://github.com/SaaShup/netbox-docker-plugin) + [Netbox Docker Agent](https://github.com/SaaShup/netbox-docker-agent) ecosystem.

```mermaid
flowchart TD
    %%%%%%%%%%%%
    %% flowchart, backup-agent connections
    %%%%%%%%%%%%
    subgraph subGraph1["Full Netbox."]
      M("Netbox docker-plugin")
      N("fa:fa-database Postgres")
      O("fa:fa-box Netbox")
      P("fa:fa-database Redis")
    end
    F[("Dump's volume")]
    AA("fa:fa-users Database Owner") --> C(["fa:fa-sun Backup-Agent"])
    A("Netbox Docker-Agent") <--> M
    C <--Create && Restore dump.--> A
    C <==List, Get, Delete && Upload a dump.====> F
    N <--Create && Restore dump.--> F
    M <--> N

    %% fill
    style C fill:#E1BEE7
    %% linkStyle
    linkStyle 1 stroke:#AA00FF,fill:none
    linkStyle 2 stroke:#AA00FF,fill:none
    linkStyle 4 stroke:#AA00FF
    linkStyle 5 stroke:#AA00FF,fill:none

```

In order to read and list dumps, *Netbox Backup Agent* must have access to the dumps. To do this, we mount the volume containing the dumps in the *Netbox Backup Agent* Docker container. This volume is also mounted in the *PostgreSQL Database* container in order to write the dumps produced by the pg_dump binary distributed in this same container.

### Create Dump

Thanks to the exec functionality exposed by the Netbox Docker Plugin, we can directly execute the dump command in the database container. In this way it is not necessary to distribute the dump binary (here with PostgreSQL `pg_dump`) with the Netbox Backup Agent.  
It should create a compressed `tar.gz` binary dump file.

```mermaid
sequenceDiagram
    title Create Dump
    actor User
    participant BackupAgent
    participant DockerAgent
    participant PostgresContainer
    participant DumpVolumePostgres

    User ->> BackupAgent : post

    BackupAgent ->> DumpVolumePostgres: Ask for dumps info
    DumpVolumePostgres ->> BackupAgent: returned info
    alt If More dumps that allowed
      BackupAgent ->> DumpVolumePostgres: remove oldest dump file
      DumpVolumePostgres ->> BackupAgent: ok!
    end

    BackupAgent ->> DockerAgent : Create_dump
    DockerAgent ->> PostgresContainer : pg_dump
    PostgresContainer ->>  DumpVolumePostgres : created_file
    DumpVolumePostgres ->> PostgresContainer : ok
    PostgresContainer ->> DockerAgent : ok
    DockerAgent ->> BackupAgent : ok

    BackupAgent ->> DockerAgent : compress_dump
    DockerAgent ->> PostgresContainer : tar -zcf
    PostgresContainer ->>  DumpVolumePostgres : compress_file
    DumpVolumePostgres ->> PostgresContainer : ok
    PostgresContainer ->> DockerAgent : ok
    DockerAgent ->> BackupAgent : ok

    BackupAgent ->> DumpVolumePostgres: remove uncompressed file
    DumpVolumePostgres ->> BackupAgent: ok!

    BackupAgent ->> User : ok
```

A retention system has been introduced to limit the number of dumps stored on the volume. By default, the system allows 10 dumps to be stored. If an eleventh dump is requested, the system will delete the oldest dump created on the volume.

### Get a Dump Content

The contents of the dumps are read directly from the storage volume.  
You can decompress with `tar -xf file-dump.tar.gz`.  
You can convert from binary file to text file with `pg_restore -f new_file.sql file.dump`.

```mermaid
sequenceDiagram
    title GET Dump
    actor User
    User ->> Backup-Agent: GetDumpContent(Dump)
    Backup-Agent ->> Volume: GetContent(file).
    Volume ->> Backup-Agent: DumpContent
    Backup-Agent ->> User: of(DumpContent)
```


### List Dumps

In the same way as for reading the contents of a dump, we directly query the volume to list the files contained in the dumps' storage directory.

```mermaid
sequenceDiagram
    title List Dumps
    actor User
    User ->> Backup-Agent: GetDumpsList()
    Backup-Agent ->> Volume: GetFiles(Directory)
    Volume ->> Backup-Agent: content
    Backup-Agent ->> User: content
```

In response to the query, User will be sent a list of URLs for the various dumps available.

### Delete a Dump

We directly delete a file from dumps container.

```mermaid
sequenceDiagram
    title Delete Dump
    actor User

    User ->> Backup-Agent: Delete Dump
    alt If file exists
      Backup-Agent ->> Volume: rm dump_file
      Volume ->> Backup-Agent: ok!
    end
    Backup-Agent ->> User: ok!
```

### Upload a Dump file

It will save file on a shared volume with Postgres container.  
It should work with a compressed '.tar.gz' file, or uncompressed binary dump file.

```mermaid
sequenceDiagram
    title Upload Dump File
    actor User
    User ->> Backup-Agent: Post
    Backup-Agent ->> Volume: Save to volume
    Volume ->> Backup-Agent: ok!
    Backup-Agent ->> User: ok!
```

### Restore Dump file

It will connect to Docker-Agent API and execute "pg_restore" command from there.  
It should work with a compressed '.tar.gz' file, or uncompressed binary dumb file.

```mermaid
sequenceDiagram
    title Restore Postgres
    actor User
    User ->> BackupAgent : post
    BackupAgent ->> DockerAgent : forward 
    alt If '.tar.gz' file
      DockerAgent ->> PostgresContainer : decompress file.tar.gz
      PostgresContainer ->>  DumpVolumePostgres : decompress
      DumpVolumePostgres ->> PostgresContainer : ok
      PostgresContainer ->> DockerAgent : ok
    end
    alt If decompressed '.dump' file
      DockerAgent ->> PostgresContainer : pg_restore
      PostgresContainer ->>  DumpVolumePostgres : select_dump_file
      DumpVolumePostgres ->> PostgresContainer : ok
      PostgresContainer ->> DockerAgent : ok
    end
    DockerAgent ->> BackupAgent : ok
    BackupAgent ->> User : ok
```

## Installation

To work, Netbox Backup Agent must:

1. be able to read and write a directory where dumps are stored
2. communicate with a Docker-agent instance where the Netbox is installed. The host where the PostgreSQL Database container resides must be correctly configured in Netbox.

Netbox Backup Agent works with the latest version of Netbox and Netbox Docker Plugin.

We provide below a docker-compose file that defines the services:

```yaml
services:
    netbox:
        image: code.europa.eu:4567/digit-c4/netbox-plugins:TAG
        container_name: netbox
        ports:
          - 8080:8080
        depends_on:
            postgres:
                condition: service_healthy
            redis:
                condition: service_started
            redis-cache:
                condition: service_started
        user: 'unit:root'
        healthcheck:
            interval: 60s
            start_period: 60s
            start_interval: 5s
            retries: 5
            test: "curl -f http://localhost:8080/api/ || exit 1"
        env_file: env/netbox.env
        volumes:
            - netbox-media-files:/opt/netbox/netbox/media
            - netbox-reports-files:/opt/netbox/netbox/reports
            - netbox-scripts-files:/opt/netbox/netbox/scripts
        environment:
            - ACTIVATE_WORKER_AND_HOUSEKEEPING=1
    postgres:
        image: docker.io/postgres:15-alpine
        env_file: env/postgres.env
        healthcheck:
            interval: 60s
            start_period: 60s
            start_interval: 10s
            retries: 5
            test: "pg_isready -h localhost -U $$POSTGRES_USER"
        container_name: netbox-postgres
        volumes:
            - netbox-postgres-data:/var/lib/postgresql/data
            - netbox-postgres-dump:/dumps
    redis:
        image: docker.io/redis:7-alpine
        env_file: env/redis.env
        command:
            - sh
            - -c # this is to evaluate the $REDIS_PASSWORD from the env
            - redis-server --appendonly yes --requirepass $$REDIS_PASSWORD ## $$ because of docker-compose
        volumes:
            - netbox-redis-data:/data
    redis-cache:
        image: docker.io/redis:7-alpine
        env_file: env/redis-cache.env
        command:
            - sh
            - -c # this is to evaluate the $REDIS_PASSWORD from the env
            - redis-server --requirepass $$REDIS_PASSWORD ## $$ because of docker-compose
        volumes:
            - netbox-redis-cache-data:/data
    netbox-docker-agent:
        image: saashup/netbox-docker-agent:TAG
        container_name: netbox-docker-agent
        ports:
            - 1880:1880
        volumes:
            - /var/run/docker.sock:/var/run/docker.sock:rw
            - netbox-docker-agent:/data
        environment:
            - ENABLE_EDITOR=true
    netbox-backup-agent:
        image: code.europa.eu:4567/digit-c4/netbox/netbox-backup-agent:TAG
        ports:
            - 1881:1880
        volumes:
            - netbox-postgres-dump:/dumps
        environment:
            - ENABLE_EDITOR=true
            - PG_CONTAINER_NAME=netbox-postgres
            - DUMP_DIRECTORY=/dumps
            - RETENTION_DUMPS=10
            - DOCKER_AGENT_BASE_URL=http://netbox-docker-agent:1880
            - DOCKER_AGENT_USER=change_this_value
            - DOCKER_AGENT_PASS=change_this_value
volumes:
    netbox-media-files:
        driver: local
    netbox-postgres-data:
        driver: local
    netbox-redis-cache-data:
        driver: local
    netbox-redis-data:
        driver: local
    netbox-reports-files:
        driver: local
    netbox-scripts-files:
        driver: local
    netbox-docker-agent:
        driver: local
    netbox-postgres-dump:
        driver: local
```

We invite you to consult [the main README.md file](../README.md) to learn about the various environment variables available.

In our diagram, we show that the Netbox Backup Agent is installed in the same Docker Host as the PostgreSQL Database container. However, it is possible to externalise the agent to another execution context, as long as the agent has access to the Dumps via a directory.

# How to Use.

Some notes when using backup-agent. Listing, creating or restoring a dump.

## How to connect to your Backup-agent.

Your Backup-agent will be connected to a Netbox, and to a Docker-agent.  
And those 3 probably will be managed from a Bubble.  

To know how to connect, you will have to connect to the managing bubble, and search
for the selected Backup-agent container.  
Once in there, you will have access to the container variables, search for variables
`API_PASSWORD_NO_BCRYPT` and `API_USERNAME` to the login credentials.

You might need to check the port to connect to Backup-agent, on the same container details.
Use the URL from a netbox, with backup-agent's port to connect.

Backup-agent login should be prompted to you, and you will be able to login with credentials you saved
from a step before.

## How to List backups

> GET /api/dump >> click `Try it out` >> click `Execute`.

## How to Create dump

> POST /api/dump >> click `Try it out` >> click `Execute`.

## How to Download a dump

**Option 1:**

e.g.
```
curl -X 'GET'  'http://yourserver.com:1881/api/dump/2025-02-11T14%3A02%3A48.007Z-psg-dump.tar.gz'  --user admin:password --output response-dump.tar.gz
```

**Option 2 ( >= v1.6.0):**

> POST /api/dump/{dump} >> click `Try it out` >> intruduce dump's name and click `Execute`.

If all went well, a `Download` bottom will appear next to the response.

## How to Restore a dump (part 1)

**Option 1, using a file from listed dumps.**

First list dumps with the following endpoint.

> GET /api/dump >> click `Try it out` >> click `Execute`.

**Option 2, uploading a file.**

Endpoint to updload file.

> POST /api/dump/{dump} >> click `Try it out` >> browse file to upload, and select a name for file >> click `Execute`.

## How to Restore a dump (part 2)

**Option 1, Restore using enpoint, recommended.**

Then restore using the following endpoint.

> POST /api/restore/{compressed_dump.tar.gz} >> click `Try it out` >> intruduce dump's name and click `Execute`.

Done!!

Extra steps in case `.dump` file is not found after decompress (It happens if tar.gz file was renamed.):

Get uncompressed name.

> GET /api/dump >> click `Try it out` >> click `Execute`.

Execute with uncompressed name.

> POST /api/restore/{UNcompressed_file.dump} >> click `Try it out` >> intruduce dump's name and click `Execute`.

**Option 2, Connect to postgres container to restore.**

```
 # list docker active containers
docker ps

 # connect to container, example
docker exec -it postgres_bckp-agt /bin/bash

 # list dumps
ls /dumps/

 # decompress if needed, example
tar -xf /dumps/2024-09-07T02:00:00.010Z-psg-dump.tar.gz

 # restore command, example
pg_restore -U netbox -d netbox -F c -c /dumps/2024-09-07T02:00:00.010Z-psg.dump
```

**Option 3, Connect to Docker-agent to restore.**

- Connect to your Docker-agent URL, and login.
- Go to containers tab.
- Search for postgres container to connect and click `exec`. A place to introduce commands will appear.

Example commands to introduce:

```
 # list dumps
ls /dumps/

 # decompress if needed, example
tar -xf /dumps/2024-09-07T02:00:00.010Z-psg-dump.tar.gz

 # restore command, example
pg_restore -U netbox -d netbox -F c -c /dumps/2024-09-07T02:00:00.010Z-psg.dump
```

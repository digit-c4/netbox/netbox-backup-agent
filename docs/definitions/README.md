# Smithy Netbox Backup Agent

## Build

To build the models in this project run:
```console
smithy clean
smithy build
```
From the root of this template directory.

The resulting OpenAPI specification can be found in the `build` directory
created by the command above in the `smithy/source/openapi` directory.

## Copy OpenAPI to specific folder on project.

```
cp build/smithy/openapi-conversion/openapi/NetboxBackupAgent.openapi.json ../../public/docs/.
```

It might need to manually change some parts of code after build on `../../public/docs/NetboxBackupAgent.openapi.json` file.

Replace `application/octet-stream` for `multipart/form-data`:

```
@@ -210,7 +210,7 @@
                 "operationId": "UploadDumpFile",
                 "requestBody": {
                     "content": {
+                        "multipart/form-data": {
-                        "application/octet-stream": {	# <<----- line to remove
                             "schema": {
                                 "$ref": "#/components/schemas/UploadDumpFileInputPayload"
                             },
```

Replace `string` for `object` and other extra lines as follow.
```
@@ -447,13 +447,8 @@
                 ]
             },
             "UploadDumpFileInputPayload": {
+                "type": "object",
+                "properties": {
+                   "fileName": {
+                      "type": "string",
+                      "format": "binary"
+                   },
+                },
-                "type": "string",		# <<----- line to remove
-                "contentEncoding": "byte"	# <<----- line to remove
             },
```


## Modify version if needed from repo.

Files with version value to modify manually.

- `models/service.smithy` or `../../public/docs/NetboxBackupAgent.openapi.json`
- `../../public/index.html`
- `../../public/docs/index.html`
- `../../package.json`

## You might need to build a new image locally to avoid testing with older images.

```
cd ../../
docker build --no-cache --progress=plain -t code.europa.eu:4567/digit-c4/netbox/netbox-backup-agent:v1.5.2 .
```

$version: "2.0"

namespace eu.europa.ec.snet.cmdb

@error("client")
@httpError(404)
structure DumpNotFoundError {
    @required
    code: Integer

    @required
    message: String
}

$version: "2.0"

namespace eu.europa.ec.snet.cmdb

use aws.protocols#restJson1

@restJson1
@title("Netbox Backup Agent")
@httpBasicAuth
service NetboxBackupAgent {
    version: "1.6.0"
    operations: [Dump, GetDumpContent, ListDumps, DeleteDump, UploadDumpFile, RestoreDumpContent, GetMetrics]
}

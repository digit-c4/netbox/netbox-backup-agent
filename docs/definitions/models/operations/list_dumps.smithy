$version: "2.0"

namespace eu.europa.ec.snet.cmdb

@readonly
@tags(["Dump"])
@http(method: "GET", uri: "/api/dump")
@documentation("Get the list of the created Dumps.")
@examples(
    [
        {
            title: "List Dumps"
            documentation: "Ask for the list of created Dump URLs."
            output: {
                dumps: [
                    "2024-06-10T12:09:06.271Z-psg-dump.tar.gz",
                    "2024-06-10T12:27:18.456Z-psg-dump.tar.gz"
                ]
            }
        }
    ]
)
operation ListDumps {
    output := {
        @required
        dumps: DumpList
    }
}

list DumpList {
    member: String
}

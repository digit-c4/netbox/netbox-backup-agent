$version: "2.0"

namespace eu.europa.ec.snet.cmdb

@readonly
@tags(["Operation"])
@http(method: "GET", uri: "/api/metrics")
@documentation("Get the Prometheus service metrics.")
@examples(
    [
        {
            title: "Get Prometheus service metrics"
            documentation: "Ask for the Prometheus service metrics."
            output: {
                contentType: "text/plain"
                content: "
####################
# Create dump
####################

# HELP success_dump_creation_total Number of success dump requested
# TYPE success_dump_creation_total counter
success_dump_creation_total 1

# HELP failed_dump_creation_total Number of dump requested but failed
# TYPE failed_dump_creation_total counter
failed_dump_creation_total 0

# HELP container_not_found_counter Number of times Database container was not found
# TYPE container_not_found_counter counter
container_not_found_counter 0

####################
# Delete dump
####################

# HELP success_deleted_dump_counter Number of dumps deleted
# TYPE success_deleted_dump_counter counter
success_deleted_dump_counter 1

# HELP failed_to_delete_dump_counter Number of times dump deletion failed
# TYPE failed_to_delete_dump_counter counter
failed_to_delete_dump_counter 0

####################
# Retrieve dump
####################

# HELP dump_downloaded_counter Number of dump calls that retrieved a dump correctly
# TYPE dump_downloaded_counter counter
dump_downloaded_counter 1

# HELP failed_dump_retrieve_counter Number of dump calls that retrieved a dump incorrectly
# TYPE failed_dump_retrieve_counter counter
failed_dump_retrieve_counter 2

####################
# Upload dump
####################

# HELP success_uploaded_dump_counter Number of dumps uploaded
# TYPE success_uploaded_dump_counter counter
success_uploaded_dump_counter 1

# HELP failed_to_upload_dump_counter Number of times dump upload failed
# TYPE failed_to_upload_dump_counter counter
failed_to_upload_dump_counter 0

####################
# Restore dump
####################

# HELP success_dump_restore_counter Number of dumps restored
# TYPE success_dump_restore_counter counter
success_dump_restore_counter 1

# HELP failed_dump_restore_counter Number of times dump restore failed
# TYPE failed_dump_restore_counter counter
failed_dump_restore_counter 2

# HELP failed_dump_restore_conn_counter Number of times dump restore connection failed
# TYPE failed_dump_restore_conn_counter counter
failed_dump_restore_conn_counter 0
"
            }
        }
    ]
)
operation GetMetrics {
    output := {
        @required
        @httpHeader("Content-Type")
        @documentation("Content-Type: text/plain")
        contentType: String = "text/plain"

        @httpPayload
        content: String
    }
}

$version: "2.0"

namespace eu.europa.ec.snet.cmdb

@tags(["Restore"])
@http(method: "POST", uri: "/api/restore/{dump}", code: 200)
@documentation("RESTORE an existing Dump on volume")
@examples(
    [
        {
            title: "Restore a Dump on volume"
            documentation: "Restore a Dump, using one from connected volume."
            input: {
                dump: "2024-05-31T14:22:05.565Z-psg-dump.tar.gz"
            }
            output: {
                message: "Restore process seems done well."
            }
        },
        {
            title: "Dump not found error"
            input: {
                dump: "notadumpfile-psg-dump.tar.gz"
            }
            error: {
                shapeId: DumpNotFoundError
                content: {
                    code: 404
                    message: "{\"stdout\":\"pg_restore: error: could not open input file \"/dumps/notadumpfile-psg-dump.tar.gz\": No such file or directory\"}"
                }
            }
        }
    ]
)
operation RestoreDumpContent {
    input := {
        @required
        @httpLabel
        dump: String
    }
    output := {
        @required
        message: String
    }
    errors: [
        DumpNotFoundError
    ]
}

$version: "2.0"

namespace eu.europa.ec.snet.cmdb

@tags(["Dump"])
@http(method: "POST", uri: "/api/dump/upload/", code: 200)
@documentation("Upload Dump file")
@examples(
    [
        {
            title: "Upload Dump file"
            documentation: "Upload a Dump from input file."
            input: {
                dump_file: "2024-05-31T14:22:05.565Z-psg-dump.tar.gz"
            }
            output: {
                message: "Uploaded filesize: 125884"
            }
        }
    ]
)
operation UploadDumpFile {
    input := {
        @required
        @httpPayload
        dump_file: Blob
    }
    output := {
        @required
        message: String
    }
}

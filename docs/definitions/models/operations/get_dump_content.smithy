$version: "2.0"

namespace eu.europa.ec.snet.cmdb

@readonly
@tags(["Dump"])
@http(method: "GET", uri: "/api/dump/{dump}")
@documentation("Get a specific Dump content with its filename")
@examples(
    [
        {
            title: "Get Dump Content"
            documentation: "Ask for a database Dump content. Output content type is `application/x-gzip`."
            input: {
                dump: "2024-12-31T22:33:44.555Z-psg-dump.tar.gz"
            }
            output: {
                content: "PGDMP..."
                content_type: "application/x-gzip"
            }
        },
        {
            title: "Dump not found error"
            input: {
                dump: "notadumpfile-psg-dump.tar.gz"
            }
            error: {
                shapeId: DumpNotFoundError
                content: {
                    code: 404
                    message: "Dump `notadumpfile-psg-dump.tar.gz` not found"
                }
            }
        }
    ]
)
operation GetDumpContent {
    input := {
        @required
        @httpLabel
        dump: String
    }
    output := {
        @required
        @httpPayload
        content: Blob

        @httpHeader("Content-Type")
        @documentation("`application/x-gzip`")
        content_type: String
    }
    errors: [
        DumpNotFoundError
    ]
}

$version: "2.0"

namespace eu.europa.ec.snet.cmdb

@tags(["Dump"])
@idempotent
@http(method: "DELETE", uri: "/api/dump/{dump}", code: 204)
@documentation("Delete a specific Dump content with its filename")
@examples(
    [
        {
            title: "Delete a dump"
            documentation: "Ask for a deleting a dump."
            input: {
                dump: "2024-12-31T22:33:44.555Z-psg-dump.tar.gz"
            }
        },
        {
            title: "Dump not found error"
            input: {
                dump: "notadumpfile-psg-dump.tar.gz"
            }
            error: {
                shapeId: DumpNotFoundError
                content: {
                    code: 404
                    message: "Dump `notadumpfile-psg-dump.tar.gz` not found"
                }
            }
        }
    ]
)
operation DeleteDump {
    input := {
        @required
        @httpLabel
        dump: String
    }
    errors: [
        DumpNotFoundError
    ]
}

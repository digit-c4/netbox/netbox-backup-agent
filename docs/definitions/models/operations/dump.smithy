$version: "2.0"

namespace eu.europa.ec.snet.cmdb

@tags(["Dump"])
@http(method: "POST", uri: "/api/dump", code: 202)
@documentation("Ask for a database Dump")
@examples(
    [
        {
            title: "Create Dump example"
            documentation: "Ask for a database dump and return the dump's URL"
            output: {
                dump: "2024-06-10T12:09:06.271Z-psg-dump.tar.gz"
            }
        }
    ]
)
operation Dump {
    output := {
        @required
        dump: String
    }
}

# Netbox Backup Agent

An agent that made backup (and run restore) of a Netbox instance.

## Start the dev environment

Requirements:
* Docker
* Node.js (LTS version) with npm
* Python v3.11 for running tests

Start Netbox stack and [Netbox Docker Agent](https://github.com/SaaShup/netbox-docker-agent):

> [!IMPORTANT]
> Netbox Docker Agent container must have read and write access to the docker unix socket (see https://github.com/SaaShup/netbox-docker-agent?tab=readme-ov-file#run)

Before starting the service, you need to install some dependencies:

```bash
npm ci
```

Then run:

```bash
docker-compose up -d
```

Got to http://localhost:1880/admin

## Run the tests

Tests are run with [Robot Framework](https://robotframework.org/)

First we need to install Robot Framework in a venv. For that in the root of the project do:

```bash
python3 -m venv venv
source venv/bin/activate
pip install robotframework robotframework-requests
```

Run the testing environment:

```bash
docker-compose -f docker-compose.testing.yml up -d
```

We use [MockServer](https://www.mock-server.com/) as Netbox backend.

Then execute tests with dev environment started:

```bash
robot tests/
```

## Build and run the Docker image

For building:

```bash
docker build -t code.europa.eu:4567/digit-c4/netbox/netbox-backup-agent .
```

Then run:

```bash
docker run -d \
  -p 1881:1880 \
  -v netbox-postgres-dump:/dumps:ro \
  --name netbox-backup-agent \
  -e ENABLE_EDITOR=true \
  -e PG_CONTAINER_NAME=netbox-postgres \
  -e DUMP_DIRECTORY=/dumps \
  -e RETENTION_DUMPS=5 \
  -e DOCKER_AGENT_BASE_URL=http://netbox-docker-agent:1880 \
  -e DOCKER_AGENT_USER=admin \
  -e DOCKER_AGENT_PASS=change_this_value \
  code.europa.eu:4567/digit-c4/netbox/netbox-backup-agent
```

> [!IMPORTANT]
> The container must share the same volume than the PostgreSQL database container (here `netbox-postgres-dump`).
> For example, if you start the agent as shown before, you have to start the PostreSQL container like that:
>
> ```bash
> docker run -d -v netbox-postgres-dump:/dumps --name netbox-postgres docker.io/postgres:15-alpine
> ```

### Environment variables

* `ENABLE_EDITOR`: if set, Node-RED editor is activated (if not present, then the editor is disable)
* `EDITOR_USERNAME`: set the editor username credential (if not set default is `admin`)
* `EDITOR_PASSWORD`: set the editor password credential (if not set default is `password`, please set it)
* `API_USERNAME`: set the API username credential (if not set default is `admin`)
* `API_PASSWORD`: set the API password credential (if not set default is `password`, please set it)
* `PG_CONTAINER_NAME`: the name of the container that host the PostgreSQL database
* `DUMP_DIRECTORY`: the directory where dumps are accessible
* `RETENTION_DUMPS`: the number of dumps the service can keep (default 10)
* `SCHEDULE_DUMP`: Activate dump auto scheduling if present. Must contain a
  valid crontab expression (see https://en.wikipedia.org/wiki/Cron, eg: `0 2 * *
  *` means: _every day at 2 am_)
* `DOCKER_AGENT_BASE_URL`: base URL for you Docker-agent instance (e.g., http://netbox-docker-agt.com:1880)
* `DOCKER_AGENT_USER`: docker-agent "username" to connect
* `DOCKER_AGENT_PASS`: docker-agent "password" to connect

> [!INFO]
> Password was generated with the bcrypt function. If you want to start the agent with a new password, you will start like this:
> `docker run ... -e API_PASSWORD=\$2a\$10\$1rLKA6cYJApby9nYaDr5GOyQXw6rXCekmelYI6xNxsWv4KktmjVIK -d -p 1881:1880 code.europa.eu:4567/digit-c4/netbox/netbox-backup-agent`

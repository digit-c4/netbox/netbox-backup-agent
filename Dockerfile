FROM nodered/node-red:4.0.8

USER root

COPY package.json /data
COPY package-lock.json /data
COPY public /data/public
COPY flows.json /data
COPY settings.js /data
COPY .config.runtime.json /data
COPY flows_cred.json /data

WORKDIR /data

RUN npm ci

WORKDIR /usr/src/node-red

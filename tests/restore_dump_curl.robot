*** Settings ***
Test Tags  Docs
Library  RequestsLibrary
LIbrary  Process

*** Variables ***

${BASE_URL}  http://localhost:1881
${USER_API}  admin
${PASSWORD_API}  thisisagoodpassword
${DOCKER_AGENT_USER}  admin
${DOCKER_AGENT_PASS}  thisisagoodpassword

*** Test Cases ***

Test restore_dump using curl.
    ${body}=  Set Variable    --data '{"docker_agt_user": "${DOCKER_AGENT_USER}", "docker_agt_pass": "${DOCKER_AGENT_PASS}"}'
    ${header}=  Set Variable    -H "Content-Type: application/json" -H "accept: */*"
    ${user}=  Set Variable    --user ${USER_API}:${PASSWORD_API}
    ${url}=  Set Variable    ${BASE_URL}/api/restore/2024-06-14T07-52-01-621Z.dump

    # curl command
    ${res0}=    Run Process    curl ${url} ${user} ${header} ${body}    shell=true
    Log to console    \ncurl ${url} \n${user} ${header} \n${body}

    Should Contain    ${res0.stdout}  Restore process seems done well.

Test decompress_dump_tar_gz using curl.
    ${body}=  Set Variable    --data '{"docker_agt_user": "${DOCKER_AGENT_USER}", "docker_agt_pass": "${DOCKER_AGENT_PASS}"}'
    ${header}=  Set Variable    -H "Content-Type: application/json" -H "accept: */*"
    ${user}=  Set Variable    --user ${USER_API}:${PASSWORD_API}
    ${url}=  Set Variable    ${BASE_URL}/api/restore/2024-06-14T07-52-01-621Z-dump.tar.gz

    # curl command
    ${res0}=    Run Process    curl ${url} ${user} ${header} ${body}    shell=true
    Log to console    \ncurl ${url} \n${user} ${header} \n${body}

    Should Contain    ${res0.stdout}  Restore process seems done well.

*** Settings ***
Test Tags  Docs
Library  RequestsLibrary
LIbrary  Process

*** Variables ***

${BASE_URL}  http://localhost:1881
${USER_API}  admin
${PASSWORD_API}  thisisagoodpassword
${DOCKER_AGENT_USER}  admin
${DOCKER_AGENT_PASS}  thisisagoodpassword
${PROJECT_PATH}  project/

*** Test Cases ***

Test upload_dump using curl.
    # if we are already into project folder, choose right path to "./"
    ${res4}=  Run Process  ls ${PROJECT_PATH} |wc -l  shell==true
    IF  '${res4.stdout}' == '0'
      ${PROJECT_PATH}  Set Variable  ./
      Log to console    ${res4.stdout}
      Log to console    ${PROJECT_PATH}
    END

    ${header}=  Set Variable    -H 'Content-Type: multipart/form-data'
    ${user}=  Set Variable    --user ${USER_API}:${PASSWORD_API}
    ${url}=  Set Variable    ${BASE_URL}/api/dump/upload/
    ${upload}=  Set Variable    -F 'file=@${PROJECT_PATH}tests/dumps/2024-06-14T07-52-01-621Z.dump'

    # curl command
    ${res0}=    Run Process    curl ${url} ${user} ${header} ${upload}   shell=true
    Log to console    \ncurl ${url} \n${user} ${header} \n${upload}

    Should Contain    ${res0.stdout}  Uploaded filesize: 13

Test delete_dump using curl.
    # if we are already into project folder, choose right path to "./"
    ${res4}=  Run Process  ls ${PROJECT_PATH} |wc -l  shell==true
    IF  '${res4.stdout}' == '0'
      ${PROJECT_PATH}  Set Variable  ./
      Log to console    ${res4.stdout}
      Log to console    ${PROJECT_PATH}
    END

    ${header}=  Set Variable    -H 'accept: */*'
    ${user}=  Set Variable    --user ${USER_API}:${PASSWORD_API}
    ${url}=  Set Variable    ${BASE_URL}/api/dump/2024-06-14T07-52-01-621Z.dump

    # curl command
    ${res0}=    Run Process    curl -X 'DELETE' ${url} ${user} ${header}   shell=true
    Log to console    \ncurl -X 'DELETE' ${url} \n${user} ${header}

    Should Be Equal    "${res0.stdout}"  ""

Test that dump is deleted
    ${auth}=  Evaluate  ("${USER_API}", "${PASSWORD_API}")
    ${response}=  DELETE  ${BASE_URL}/api/dump/2024-06-14T07-52-01-621Z.dump  auth=${auth}  expected_status=404

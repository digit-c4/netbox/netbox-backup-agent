*** Settings ***
Test Tags  Dump
Library  RequestsLibrary

*** Variables ***

${BASE_URL}  http://localhost:1880
${USER_API}  admin
${PASSWORD_API}  thisisagoodpassword

@{dumps_list}    2024-06-14T07-52-01-621Z.dump    2024-06-14T07-52-01-621Z-dump.tar.gz

*** Test Cases ***

Test that we can list Dumps

    ${auth}=  Evaluate  ("${USER_API}", "${PASSWORD_API}")
    ${response}=  GET  ${BASE_URL}/api/dump  auth=${auth}  expected_status=200

    ${cnt}=    Get length    ${response.json()}[dumps]
    Log to console   \nDumps_count: ${cnt}
    should be equal as numbers  ${cnt}  2

    Log to console   \nDumps: ${dumps_list}
    Log to console   \nDumps: ${response.json()}[dumps]

    Should Contain  ${response.json()}[dumps]  ${dumps_list}[0]
    Should Contain  ${response.json()}[dumps]  ${dumps_list}[1]


*** Settings ***
Test Tags  Dump
Library  RequestsLibrary

*** Variables ***

${BASE_URL}  http://localhost:1880
${USER_API}  admin
${PASSWORD_API}  thisisagoodpassword

*** Test Cases ***

Test that we can get the dump content
    ${auth}=  Evaluate  ("${USER_API}", "${PASSWORD_API}")
    ${response}=  GET  ${BASE_URL}/api/dump/2024-06-14T07-52-01-621Z.dump  auth=${auth}  expected_status=200
    Should Be Equal  ${response.text}  DUMP CONTENT\n

*** Settings ***
Test Tags  Dump
Library  RequestsLibrary

*** Variables ***

${BASE_URL}  http://localhost:1880
${USER_API}  admin
${PASSWORD_API}  thisisagoodpassword

*** Test Cases ***

Test that we can get the Prometheus metrics content
    ${auth}=  Evaluate  ("${USER_API}", "${PASSWORD_API}")
    ${response}=  GET  ${BASE_URL}/api/metrics  auth=${auth}  expected_status=200

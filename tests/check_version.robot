*** Settings ***
Test Tags  Docs
Library  RequestsLibrary
LIbrary  Process

*** Variables ***

${BASE_URL}  http://localhost:1880
${USER_API}  admin
${PASSWORD_API}  thisisagoodpassword
${PROJECT_PATH}  project/

*** Test Cases ***

Test tag versions, written down on docs/.
    ${auth}=  Evaluate  ("${USER_API}", "${PASSWORD_API}")
    ${response_home}=  GET  ${BASE_URL}  auth=${auth}  expected_status=200
    ${response}=  GET  ${BASE_URL}/docs  auth=${auth}  expected_status=200

    # first line with tag version
    ${res0}=    Run Process    echo '${response.text}'|grep 'Backup Agent'| awk '$0 {print $7}'| sed 's/.$//' |head -n 1    shell=true
    # second line with tag version
    ${res1}=    Run Process    echo '${response.text}'|grep 'Backup Agent'| awk '$0 {print $7}'| sed 's/..title.$//' | tail -n 1    shell=true
    # home_page tag version
    ${res2}=    Run Process    echo '${response_home.text}'|grep 'Backup Agent'| awk '$0 {print $5}'| sed 's/..title.$//' | head -n 1    shell=true
    # second line home_page tag version
    ${res3}=    Run Process    echo '${response_home.text}'|grep 'Backup Agent'| awk '$0 {print $5}'| sed 's/..h1.$//' | tail -n 1    shell=true

    Log to console    \n${res0.stdout}, path: public/docs/index.html
    Log to console    \n${res1.stdout}, path: public/docs/index.html
    Log to console    \n${res2.stdout}, path: public/index.html
    Log to console    \n${res3.stdout}, path: public/index.html

    Should Be Equal    ${res0.stdout}  ${res1.stdout}
    Should Be Equal    ${res0.stdout}  ${res2.stdout}
    Should Be Equal    ${res0.stdout}  ${res3.stdout}

Test versions on different files.
    ${auth}=  Evaluate  ("${USER_API}", "${PASSWORD_API}")
    ${response}=  GET  ${BASE_URL}/docs  auth=${auth}  expected_status=200
    # first line with tag version
    ${res0}=    Run Process    echo '${response.text}'|grep 'Backup Agent'| awk '$0 {print $7}'| sed 's/.$//' |sed 's/^.//' |head -n 1    shell=true
    Log to console    \n${res0.stdout}

    # if we are already into project folder, choose right path to "./"
    ${res4}=  Run Process  ls ${PROJECT_PATH} |wc -l  shell==true
    IF  '${res4.stdout}' == '0'
      ${PROJECT_PATH}  Set Variable  ./
      Log to console    ${res4.stdout}
      Log to console    ${PROJECT_PATH}
    END

    # text on file
    ${res1}=    Run Process    cat ${PROJECT_PATH}docs/definitions/models/service.smithy |grep Backup -A 1    shell=true
    ${res2}=    Run Process    cat ${PROJECT_PATH}package.json |grep backup -A 1   shell=true
    ${res3}=    Run Process    cat ${PROJECT_PATH}public/docs/NetboxBackupAgent.openapi.json |grep Backup -A 1   shell=true

    Log to console    Testing ${res0.stdout} on: ${PROJECT_PATH}docs/definitions/models/service.smithy
    Should Contain    ${res1.stdout}  ${res0.stdout}

    Log to console    Testing ${res0.stdout} on: ${PROJECT_PATH}package.json
    Should Contain    ${res2.stdout}  ${res0.stdout}

    Log to console    Testing ${res0.stdout} on: ${PROJECT_PATH}public/docs/NetboxBackupAgent.openapi.json
    Should Contain    ${res3.stdout}  ${res0.stdout}

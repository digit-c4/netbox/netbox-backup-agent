# How to test locally

Create environment.
```
docker build -t robotframework ./tests/docker/
docker image rm netbox-backup-agent-netbox-backup-agent
docker compose -f docker-compose.testing.yml up -d
```

Run tests inside docker container.
```
docker run --rm -v $PWD:/project \
--network netbox-backup-agent_netbox-backup-agent \
robotframework:latest bash -c \
"robot --outputdir /project/reports \
-v BASE_URL:http://netbox-backup-agent:1880 \
-v MOCKSERVER_BASE_URL:http://mock-docker-agent:1080 \
/project/tests/"
```

Clean environment.
```
docker compose -f docker-compose.testing.yml down -v
docker image rm netbox-backup-agent-netbox-backup-agent
docker image rm robotframework
```

# How to test against a remote server

e.g.:

```
robot \
-v BASE_URL:http://mylab.d.publicvm.com:1881 \
-v PASSWORD_API:b6fakePasswordForBackupAgent \
-v DOCKER_AGENT_USER:<user> \
-v DOCKER_AGENT_PASS:<pass> \
tests/upload_dump_curl.robot
```

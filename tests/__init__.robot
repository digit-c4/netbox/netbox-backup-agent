*** Settings ***
Suite Setup  Reset Mockserver
Library  RequestsLibrary

*** Variables ***

${MOCKSERVER_BASE_URL}  http://localhost:1080

*** Keywords ***

Reset Mockserver
    ${response}=  PUT  ${MOCKSERVER_BASE_URL}/mockserver/clear  params=type=log  expected_status=200
